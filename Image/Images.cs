﻿////////////////////////////////////////////////////////////////////////////////////////////////////
// file:	Images.cs
//
// summary:	Implements the images class
////////////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageProcess
{
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    /// <summary>   An images. </summary>
    ///
    /// <remarks>   Viki, 8/1/2018. </remarks>
    ////////////////////////////////////////////////////////////////////////////////////////////////////

    public abstract class Images
    {  
        /// <summary>   Full pathname of the source file. </summary>
        protected string srcPath;

        /// <summary>   Full pathname of the destination file. </summary>
        protected string destPath;
        
        /// <summary>   The type. </summary>
        protected string type;

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>   Constructor. </summary>
        ///
        /// <remarks>   Viki, 22/1/2018. </remarks>
        ///
        /// <param name="srcPath">  Full pathname of the source file. </param>
        /// <param name="destPath"> Full pathname of the destination file. </param>
        /// <param name="type">     The type. </param>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        public Images(string srcPath, string destPath, string type)
        {
            this.srcPath = srcPath;
            this.destPath = destPath;
            this.type = type;
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>   Executes this object. </summary>
        ///
        /// <remarks>   Viki, 22/1/2018. </remarks>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        public abstract void Execute();
    }
}
