﻿////////////////////////////////////////////////////////////////////////////////////////////////////
// file:	Resizer\BaseResize.cs
//
// summary:	Implements the base resize class
////////////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageProcess.Resizer
{
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    /// <summary>   A base resize. </summary>
    ///
    /// <remarks>   Viki, 22/1/2018. </remarks>
    ////////////////////////////////////////////////////////////////////////////////////////////////////

    public abstract class BaseResize
    {
        /// <summary>   Full pathname of the source file. </summary>
        protected string srcPath;

        /// <summary>   Full pathname of the destination file. </summary>
        protected string destPath;

        /// <summary>   The width. </summary>
        protected int width;

        /// <summary>   The height. </summary>
        protected int height;

        /// <summary>   The x coordinate. </summary>
        protected int x;

        /// <summary>   The y coordinate. </summary>
        protected int y;

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>   Constructor. </summary>
        ///
        /// <remarks>   Viki, 22/1/2018. </remarks>
        ///
        /// <param name="srcPath">  Full pathname of the source file. </param>
        /// <param name="destPath"> Full pathname of the destination file. </param>
        /// <param name="width">    The width. </param>
        /// <param name="height">   The height. </param>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        public BaseResize(string srcPath, string destPath, int width, int height)
        {
            this.srcPath = srcPath;
            this.destPath = destPath;
            this.width = width;
            this.height = height;
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>   Constructor. </summary>
        ///
        /// <remarks>   Viki, 22/1/2018. </remarks>
        ///
        /// <param name="srcPath">  Full pathname of the source file. </param>
        /// <param name="destPath"> Full pathname of the destination file. </param>
        /// <param name="width">    The width. </param>
        /// <param name="height">   The height. </param>
        /// <param name="x">        The x coordinate. </param>
        /// <param name="y">        The y coordinate. </param>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        public BaseResize(string srcPath, string destPath, int width, int height, int x, int y)
        {
            this.srcPath = srcPath;
            this.destPath = destPath;
            this.width = width;
            this.height = height;
            this.x = x;
            this.y = y;
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>   Executes this object. </summary>
        ///
        /// <remarks>   Viki, 22/1/2018. </remarks>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        public abstract void Execute();
    }
}
