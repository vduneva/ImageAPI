﻿////////////////////////////////////////////////////////////////////////////////////////////////////
// file:	Resizer\Resize.cs
//
// summary:	Implements the resize class
////////////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageProcess.Resizer
{
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    /// <summary>   A resize. </summary>
    ///
    /// <remarks>   Viki, 22/1/2018. </remarks>
    ////////////////////////////////////////////////////////////////////////////////////////////////////

    public class Resize : Images
    {
       
        /// <summary>   The width. </summary>
        public int width;

        /// <summary>   The height. </summary>
        public int height;

        /// <summary>   The x coordinate. </summary>
        public int x;

        /// <summary>   The y coordinate. </summary>
        public int y;

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>   Constructor. </summary>
        ///
        /// <remarks>   Viki, 22/1/2018. </remarks>
        ///
        /// <param name="srcPath">  Full pathname of the source file. </param>
        /// <param name="destPath"> Full pathname of the destination file. </param>
        /// <param name="type">     The type. </param>
        /// <param name="width">    The width. </param>
        /// <param name="height">   The height. </param>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        public Resize(string srcPath, string destPath, string type, int width, int height) : base(srcPath, destPath, type)
        {
            this.width = width;
            this.height = height;
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>   Constructor. </summary>
        ///
        /// <remarks>   Viki, 22/1/2018. </remarks>
        ///
        /// <param name="srcPath">  Full pathname of the source file. </param>
        /// <param name="destPath"> Full pathname of the destination file. </param>
        /// <param name="type">     The type. </param>
        /// <param name="width">    The width. </param>
        /// <param name="height">   The height. </param>
        /// <param name="x">        The x coordinate. </param>
        /// <param name="y">        The y coordinate. </param>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        public Resize(string srcPath, string destPath, string type, int width, int height, int x, int y) : base(srcPath, destPath, type)
        {
            this.width = width;
            this.height = height;
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>   Executes this object. </summary>
        ///
        /// <remarks>   Viki, 22/1/2018. </remarks>
        ///
        /// <exception cref="UnsupportedTypeException">     Thrown when an Unsupported Type error
        ///                                                 condition occurs.
        /// </exception>
        /// <exception cref="MyFileNotFoundException">      Thrown when My File Not Found error condition
        ///                                                 occurs.
        /// </exception>
        /// <exception cref="SourceAndDestPathException">   Thrown when a Source And Destination Path
        ///                                                 error condition occurs.
        /// </exception>
        /// <exception cref="UnsupportedDestPathException"> Thrown when an Unsupported Destination Path
        ///                                                 error condition occurs.
        /// </exception>
        /// <exception cref="MyOutOfMemoryException">       Thrown when My Out Of Memory error condition
        ///                                                 occurs.
        /// </exception>
        /// <exception cref="MySystemException">            Thrown when My System error condition occurs.
        /// </exception>
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        public override void Execute()
        {
            Context context = new Context();
            try
            {
                switch (type.ToLower())
                {
                    case "crop":
                        context.SetStrategy(new Crop(srcPath, destPath, width, height, x, y));
                        break;
                    case "skew":
                        context.SetStrategy(new Skew(srcPath, destPath, width, height));
                        break;
                    case "keepaspect":
                        context.SetStrategy(new KeepAspect(srcPath, destPath, width, height));
                        break;
                    default:
                        throw new Exceptions.UnsupportedTypeException(type + " is invalid resizer type. Supported types: skew, crop and keepaspect");
                }

                context.Execute();
            }
            catch (FileNotFoundException)
            {
                throw new Exceptions.MyFileNotFoundException("File with path  " + srcPath + " not found.");
            }
            catch (ArgumentException)
            {
                throw new Exceptions.SourceAndDestPathException("Source or Destination path is an empty string, null or contains invalid characters.");
            }
            catch (NotSupportedException)
            {
                throw new Exceptions.UnsupportedDestPathException("Destination path " + destPath + " is in an invalid format.");
            }
            catch (OutOfMemoryException)
            {
                throw new Exceptions.MyOutOfMemoryException("There is not enough memory to continue the execution of a program");
            }
            catch (SystemException)
            {
                throw new Exceptions.MySystemException("There is a problem with source path or destionation path.");
            }

        }
    }
}
